var arr = [];
function addnumbertoArray() {
    var n = document.getElementById("number-n").value * 1;
    arr.push(n);
    document.getElementById("result-array").innerHTML = `<p>${arr}</p>`
}
// bai1
function tinhtong() {
    var tong = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > 0) {
            tong += arr[i];
        }
    }
    document.getElementById("result-b1").innerHTML = `<p>Tổng các số dương: ${tong}</p>`
}
// bai2
function demsoduong() {
    var count = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > 0) {
            count++;
        }
    }
    document.getElementById("result-b2").innerHTML = `<p>Số dương: ${count}</p>`
}
// bai3
function timsonhonhat() {
    var min = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    document.getElementById("result-b3").innerHTML = `<p>Số nhỏ nhất: ${min}</p>`
}
// bai4
function timsoduongnhonhat() {
    var mangsoduong = [];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > 0) {
            mangsoduong.push(arr[i])
        }
    }
    var min = mangsoduong[0];
    for (var j = 1; j < mangsoduong.length; j++) {
        if (mangsoduong[j] < min) {
            min = mangsoduong[j];
        }
    }
    if (min == undefined) {
        document.getElementById("result-b4").innerHTML = `<p>không có số dương nhỏ nhất</p>`
    } else {
        document.getElementById("result-b4").innerHTML = `<p>Số dương nhỏ nhất: ${min}</p>`
    }

}
// bai5
function timsochancuoicung() {
    var sochancuoicung = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] % 2 == 0) {
            sochancuoicung = arr[i];
        }
    }
    document.getElementById("result-b5").innerHTML = `<p>Số chẵn cuối cùng: ${sochancuoicung}</p>`
}
// bai6
function doicho() {
    var vitri1 = document.getElementById("txt-vi-tri-so-1").value * 1;
    var vitri2 = document.getElementById("txt-vi-tri-so-2").value * 1;

    var bientam = arr[vitri1]
    arr[vitri1] = arr[vitri2];
    arr[vitri2] = bientam;
    document.getElementById("result-b6").innerHTML = `<p>Mảng sau khi đổi: ${arr}</p>`
}
// bai7
function sapxeptangdan() {
    var currentvalue = 0;
    for (var i = 0; i < arr.length; i++) {
        currentvalue = i;
        for (var j = i + 1; j < arr.length; j++) {
            if (arr[currentvalue] > arr[j]) {
                currentvalue = j;
            }

        }
        if (i !== currentvalue) {
            var bientam = arr[i];
            arr[i] = arr[currentvalue];
            arr[currentvalue] = bientam;
        }
    }

    document.getElementById("result-b7").innerHTML = `<p>Mảng sau khi sắp xếp: ${arr}</p>`
}
// bai8
function kiemtrasonguyento(n) {
    if (n < 2) {
        return false;
    }
    for (var r = 2; r < Math.sqrt(n); r++) {
        if (n % r == 0)
            return false;
    }
    return true;
}


function timsonguyentodautien() {
    var songuyento = 0;
    for (var i = 0; i < arr.length; i++) {
        if (kiemtrasonguyento(arr[i])) {
            songuyento = arr[i];
            break;
        }
    }
    document.getElementById("result-b8").innerHTML = `<p>Số nguyên tố đầu tiên:  ${songuyento}</p>`
}
// bai9
var arrb9 = [];
function addnumbertoArrayb9() {
    var n = document.getElementById("numberb9").value * 1;
    arrb9.push(n);
    document.getElementById("result-arrayb9").innerHTML = `<p>${arrb9}</p>`
}

function demsonguyen() {
    var count = 0;
    for (var i = 0; i < arrb9.length; i++) {
        if (Number.isInteger(arrb9[i])) {
            count++;
        }
    }
    document.getElementById("result-b9").innerHTML = `<p>Số nguyên:  ${count}</p>`
}
// bai10
function sosanh() {
    var countsoduong = 0;
    var countsoam = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > 0) {
            countsoduong++;
        }
    }
    for(var j=0;j<arr.length;j++){
        if(arr[j] < 0){
            countsoam++;
        }
    }

    if (countsoduong > countsoam) {
        document.getElementById("result-b10").innerHTML = `<p>Số dương > Số âm</p>`
    } else if (countsoam > countsoduong) {
        document.getElementById("result-b10").innerHTML = `<p>Số dương < Số âm</p>`
    } else {
        document.getElementById("result-b10").innerHTML = `<p>Số dương = Số âm</p>`
    }
}